'use strict'

class UsersController extends @BaseCtrl
	@register 'UsersController'
	@inject '$scope', '$http', '$animate', '$location', 'User'



	initialize: ->
		@$scope.hasTeamCode = false
		@$scope.createNewTeam = false
		@$animate.enabled(true)
		usr = @User.currentUser
		if usr
			@$scope.user = usr
		

	
	showTeamCode: -> 
		@$scope.hasTeamCode = true
		@$scope.createNewTeam = false
		
	showCreateTeam: -> 
		@$scope.createNewTeam = true
		@$scope.hasTeamCode = false

	login: ->
		console.log "Logging in User : #{JSON.stringify(@$scope.user)}"
		usr = @User
		loc = @$location
		@$http.post '/login', @$scope.user
			.success (data, status, headers, config) ->
				console.log "Logged In #{JSON.stringify(data)}"
				usr.currentUser = data
				loc.path('/users/me')
			.error (data, status, headers, config) ->
				console.log "Problemo!"

	submit: ->
		user = new @User()
		for key, val of @$scope.user
			user[key] = val
		_u = @User
		_loc = @$location
		@User.save user, (usr) ->
			console.log "save user returned #{JSON.stringify(usr)}"
			_u.currentUser = usr
			_loc.path '/users/me'

		