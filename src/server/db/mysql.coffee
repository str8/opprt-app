'use strict'

class MySQL


	constructor: () ->
		
		@mysql = require 'mysql'
		@pool = @mysql.createPool 
			host: 'localhost'
			user: 'root'
			password: ''
			database: 'strate_db'
	db: () =>
		@pool

	create: (tableName, data, callback) =>
		console.log "Setting data : #{JSON.stringify(data)}"
		query = "INSERT INTO #{tableName} SET ?"
		iq = @pool.query query, data, (err, result) ->
			console.log "SQL :: #{@sql}"
			if err
				console.log "Error'd out : #{err.code} :: #{err.stack}"
				callback err, null
			else
				console.log "Inserted a #{tableName} record ==> #{JSON.stringify(result)}"
				data['@rid'] = result.insertId
				console.log "Returning #{tableName} : #{JSON.stringify(data)}"
				callback null, data
		# console.log iq.sql

	edge: (from, to, name, callback) =>
		query = "INSERT INTO #{name} set `from` = ?, `to` = ?"
		@pool.query query, [from, to], (err, result) ->
			if err
				callback err, null
			else
				callback null, {'@rid':result.insertId}

	find: (tableName, fieldName, fieldValue, callback) =>
		query = "SELECT * FROM #{tableName} WHERE #{fieldName}='#{fieldValue}'"
		@findOne query, callback
		

	findById: (tableName, id, callback) =>
		query = "SELECT * FROM #{tableName} WHERE ID=#{id}"
		@findOne query, callback

	findOne: (query, callback) =>
		@pool.query query, (err, rows) ->
			console.log "Error #{err} or Rows #{rows}"
			if err
				callback err, null
			else if !rows || rows.length == 0
				callback new Error("No Records Found"), null
			else
				callback null, MySQL.camelize(rows[0])

	# Statics
	@changeCase = require 'change-case'
	@camelize: (data) ->
		cData = {}
		for key,val of data
			cKey = @changeCase.camel key
			cData[cKey] = val
		cData








module.exports = exports = new MySQL