'use strict';

opprtServices = angular.module('opprtServices', ['ngResource']);
opprtServices.factory 'Users', ['$resource'], ($resource) ->
	return $resource '/users/all', {},{
		query:
			method:'GET'
	}