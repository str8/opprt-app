'use strict'

class Database
	constructor: () ->
		@oriento = require 'oriento'
		@server = @oriento 
			host: 'localhost'
			port: 2424
			username: 'root'
			password: 'password'
	list: () =>
		return @server.list()
	db: () =>
		@dbs = @server.use('test_graph')
		return @dbs

module.exports = exports = new Database