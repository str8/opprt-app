'use strict'

module.exports = (app, passport) ->
	console.log "Loading routes ..."

	isLoggedIn = (req, res, next) ->
		if req.isAuthenticated()
			# console.log "User in Req: #{JSON.stringify(req.user)}"
			next()
		else
			console.log "Unauthenticated access attemped ==> #{req.url}"
			res.redirect('/')


	app.get '/', (req, res) ->
		res.render 'layout'

	app.get '/partials/:name', (req, res) ->
		console.log "recvd request for partial : #{req.params.name}"
		res.render "partials/#{req.params.name}"

	app.get '/login', (req, res) ->
		res.render 'login.jade', {message: req.flash('loginMessage')}

	app.get '/signup', (req, res) ->
		res.render 'signup.jade', {message: req.flash('signupMessage')}

	app.post '/users', passport.authenticate 'local-signup', {
		successRedirect: '/users/me'
		failureRedirect: '/signup'
		failureFlash: true
	}

	app.post '/login', passport.authenticate 'local-login', {
		successRedirect: '/users/me'
		failureRedirect: '/login'
		failureFlash: true
	}

	app.get '/profile', isLoggedIn, (req, res) ->
		res.render 'profile.jade', {user: req.user}

	app.get '/logout', (req, res) ->
		req.logout()
		res.redirect('/')

	users = require '../routers/users'
	app.use '/users', isLoggedIn, users
	app



	
