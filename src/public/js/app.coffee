'use strict'

app = angular.module('opprt', ['ngResource', 'ngAnimate', 'ngRoute', 'opprtRoutes', 'opprtResources'])
  
 
class @BaseCtrl
  @register: (name) ->
    console.log "App is #{app}"
    name ?= @name || @toString().match(/function\s*(.*?)\(/)?[1]
    console.log "registering controller #{name}"
    app.controller name, @
    console.log "registred controller #{name}"
 
  @inject: (args...) ->
    console.log "Injecting..."
    @$inject = args
 
  constructor: (args...) ->
    for key, index in @constructor.$inject
      @[key] = args[index]
 
    for key, fn of @constructor.prototype
      continue unless typeof fn is 'function'
      continue if key in ['constructor', 'initialize'] or key[0] is '_'
      @$scope[key] = fn.bind?(@) || _.bind(fn, @)
 
    @initialize?()

