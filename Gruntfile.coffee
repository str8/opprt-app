module.exports = (grunt) -> 
  require('load-grunt-tasks')(grunt)
  paths=
    js: 'node_modules/angular/**/*.js'
 
  grunt.initConfig
    compass:
      dist:
        options:
          sassDir: 'sass'
          cssDir: 'css'
    coffee:
        server:
          expand: true
          cwd: 'src/server'
          src: ['**/*.coffee']
          dest: 'target'
          ext: '.js'
        client:
          expand: true
          cwd: 'src/public'
          src: '**/*.coffee'
          dest: 'target/public'
          ext: '.js'
          
    copy:
      css:
        cwd: 'src/public'
        src: '**/*.css'
        dest: 'target/public'
        expand:true
      js:
        cwd: 'src/public'
        src: '**/*.js'
        dest: 'target/public'
        expand:true
      jade:
        cwd: 'src/server'
        src:['**/*.jade']
        dest:'target'
        expand:true
      angular:
        cwd: 'node_modules/angular'
        src:['angular.js']
        dest:'target/public/js'
        expand:true
      angularAnimate:
        cwd: 'node_modules/angular-animate'
        src:['angular-animate.js']
        dest:'target/public/js'
        expand:true
      angularRoute:
        cwd: 'node_modules/angular-route'
        src:['angular-route.js']
        dest:'target/public/js'
        expand:true
      angularResource:
        cwd: 'node_modules/angular-resource'
        src:['angular-resource.js']
        dest:'target/public/js'
        expand:true
      fonts:
        cwd: 'src/public/fonts'
        src: '**/*'
        dest: 'target/public/fonts'
        expand:true


    jade:
      compile:
        options:
          client:true
          pretty:true
        cwd: 'src/client'
        src: '**/*.jade'
        dest: 'target'
        ext: '.html'
        expand:true
      
        
    express: 
      web: 
        options: 
          script: 'target/server.js',
    watch: 
      css:
        files:['src/public/**/*.css']
        tasks:['copy:css']
      coffeeServer:
        files:['src/server/**/*.coffee']
        tasks:['coffee:server']
      coffeeClient:
        files:['src/public/**/*.coffee']
        tasks:['coffee:client']
      jadeFiles:
        files:['src/server/**/*.jade']
        tasks:['copy:jade']
      frontend: 
        options: 
          livereload: true
        files: [
          'target/**/*.jade'
          'target/public/**/*.js'
          'target/public/**/*'
        ]
      stylesSass: 
        files: ['src/public/scss/*.scss']
        tasks: ['compass']
      web: 
        files: [
          'target/**/*.js'
          'config/*'
          'test/**/*.js'
        ]
        tasks: ['express:web']
        options: 
          nospawn: true
          atBegin: true
    parallel: 
      web: 
        options: 
          stream: true
        tasks:[
          {
            grunt:true
            args:['watch:coffeeServer']
          }
          {
            grunt:true
            args:['watch:coffeeClient']
          }
          {
            grunt:true
            args:['watch:jadeFiles']
          }
          {
            grunt: true
            args: ['watch:frontend']
          } 
          {
            grunt: true
            args: ['watch:stylesSass']
          }
          {
            grunt: true
            args: ['watch:web']
          }
          {
            grunt: true
            args: ['watch:css']
          }
        ]
      
  
  grunt.registerTask 'web', 'launch webserver and watch tasks', ['coffee','jade','parallel:web']
  grunt.registerTask 'default', ['coffee','jade','copy','web']
