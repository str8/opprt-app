'use strict'
Model = require '../core/model'
Team = require './team'
bcrypt = require 'bcrypt-nodejs'


# To create an invite code based on user & team ==>
# S = require 'string'
# H = require 'hashids'
# keys = S('#11:23').chompLeft('#').split(':').map(Number).concat(S('#12:24').chompLeft('#').split(':').map(Number))
# h = new H("mycoolsaltgoeshere")
# invite_code = h.encode(keys)
# now store this in db with user rid & team rid
# while extracting during signup - ensure that h.decode(invite_code) translates eventually to user & team rids

# Figure out the 'right' way to define model data
# Right now, we are 'assuming' the data model, we should have it specified.
# --- Figured this out - the data model is defined in the 'fields' array
class User extends Model
	merge = require('merge')
	RID = '@rid'
	dbFields = ['name', 'email', 'password']
	fields = ['name', 'email', 'password','teamId']


	constructor: (data) -> 
		@temp_data = data
		super data, 'User', fields, dbFields

		

	initialize: ->
		@tableName = 'User'

	save: (callback) =>
		temp_pw = @data.password
		if @data.password
			@data.password = @_generateHash @data.password
		console.log "Saving User with Team Name #{@data.teamName}"
		super (err, u) ->
			console.log "Handing back to callback"
			callback err, u

	createUserForTeam: (teamName, callback) =>
		localUser = @
	


	createUserAndTeam: (teamName, callback) =>
		localUser = @
		console.log "Creating User and Team : #{teamName}, #{@data.email}"
		# console.log "#{@newTeamNewUser(team)}"
		team = new Team({name:teamName})
		team.save (err, t) ->
			if err
				console.log "Error creating Team : #{err}"
				callback err, null
			else
				console.log "Team Saved : #{JSON.stringify(t)}"
				localUser.save (err1, u) ->
					if err1
						console.log "Error : #{err1}"
						callback err, u
					else
						console.log "User saved : #{JSON.stringify(u)}"
						console.log "Adding to team : #{JSON.stringify(t)} / #{t[RID]}"
						Model.mysql.edge u[RID], t[RID], 'leader_of', (err2, edge) ->
							if err2
								callback err2, null
							else
								callback null, u
						
		

	_generateHash: (password) =>
		bcrypt.hashSync password, bcrypt.genSaltSync(8), null

	validPassword: (password) =>
		console.log "Comparing #{password} with #{@data.password} in #{JSON.stringify(@data)}"
		bcrypt.compareSync password, @data.password

	newTeamNewUser: (teamName) =>
		"create edge LeaderOf from (create vertex User #{JSON.stringify(@data)}) to (create vertex Team set name='#{teamName}')"


	@findByEmail= (email, callback) ->
		Model.mysql.find 'user', 'email', email, (err, u) ->
			if err
				callback err, null
			else
				console.log "User extracted : #{JSON.stringify(u)}" 
				user = new User(u)
				console.log "User Callback init"
				callback null, user

		# Model.db.select().from('User').where({email:email}).one()
		# 	.then (u) ->
		# 		if u
		# 			# console.log "Got User from DB : #{JSON.stringify(u)}"
		# 			user = new User(u)
		# 			callback(null, user)
		# 		else
		# 			callback null, null
		# 	.catch (err) ->
		# 		callback(err, null)

	@findById: (id, callback) ->
		Model.mysql.findById 'user', id, (err, u) ->
			if err
				callback err, null
			else
				user = new User(u)
				callback null, user.data

		# @db.select().from('User').where({'@rid':id}).one()
		# 	.then (u) ->
		# 		callback(null, u)
		# 	.catch (err) ->
		# 		callback(err, null)





		
		

module.exports = exports = User