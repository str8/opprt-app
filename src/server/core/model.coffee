'use strict'

class @Model
	merge = require('merge')
	RID = '@rid'
	# @dbServer = require './db/database'
	# @db = @dbServer.db()
	@mysql = require '../db/mysql'

	constructor: (data, name, fields, dbFields) ->
		@tableName = name
		@fields = fields
		@dbFields = dbFields
		@data = {}
		@dbData = {}
		@setData data
		console.log "Setup Data as : #{JSON.stringify(@data)}"
			
		@initialize?()

	setData: (data) =>
		for key in @fields
			if(data[key])
				@data[key] = data[key]
		# Set ID - RID for orient (remove later)
		if(data[RID])
			@data[RID] = data[RID]
		if data['id']
			@data[RID] = data['id']

	getData: () =>
		for key in @dbFields
			if(@data[key])
				@dbData[key] = @data[key]
		@dbData

	save: (callback) =>
		setData = @setData
		Model.mysql.create @tableName, @getData(), callback
		# Model.db.insert().into(@tableName).set(@data).one()
		# 	.then (u) ->
		# 		setData u
		# 		callback(null, u)
		# 	.catch (err) ->
		# 		callback(err, null)

		# .then (u) ->
		# 	@data = merge(@data, u)
			# # # console.log "Res is: #{resp}"
		# 	testFunc
		# .then (t) ->
		# 	t()
		# 	# # # console.log "Created #{@tableName} with RecordID : #{@data[RID]}"
		# 	# res.status 201
		# 	# res.json(u)
		# .catch (error) ->
		# 	# # console.log "Error'd out mate : #{error}"
		# 	if error.type == "com.orientechnologies.orient.core.storage.ORecordDuplicatedException"
		# 		res.status 409
		# 		res.send "Duplicate Index Value"
		# 	else
		# 		res.status 500
		# 		res.json error

	

module.exports = exports = @Model


	