'use strict'

express = require 'express'
app = express()

passport = require 'passport'
flash = require 'express-flash'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'
session = require 'express-session'
oriento = require './db/database'

db = oriento.db()

require('./core/passport')(passport)


app.use cookieParser('oppristheonlywaytoopprtandr8')
app.use bodyParser.urlencoded()
app.use bodyParser.json()

app.set 'views', __dirname + '/views'
app.set 'view engine', 'jade'

app.use session()
app.use passport.initialize()
app.use passport.session()
app.use flash()


# Routes Setup
routes = require './core/routes'

# First, index & partials setup


routes(app, passport)





app.use '/', express.static __dirname + '/public'
# app.use '/', express.static __dirname + '/lib'

 
server = app.listen 3000, () ->
	console.log "Server listening on port #{server.address().port}"

