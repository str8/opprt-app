'use strict'
	
express = require 'express'
controller = require '../controllers/users'
router = express.Router()

router.get '/', (req, res) ->
	controller.all()
	res.send "we will show you our users mateyyy!"

router.get '/me', (req, res) ->
	res.json(req.user)


# router.post '/', passport.authenticate 'local-signup', {
# 		successRedirect: '/users/me'
# 		failureRedirect: '/signup'
# 		failureFlash: true
# 	}

# router.post '/', (req, res) ->
# 	controller.create req.body, res

router.get '/:firstName/:lastName/:email', (req, res) ->
	controller.create()
	res.send "We'll create a User"

router.post '/:id/up', (req, res) ->
	console.log "Rating up #{req.params.id}"

module.exports = exports = router