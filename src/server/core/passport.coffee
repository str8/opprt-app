'ust strict'

LocalStrategy = require('passport-local').Strategy
User = require '../models/user'

module.exports = (passport) ->
	passport.serializeUser (user, done) ->
		console.log "Serializing User : #{JSON.stringify(user)}"
		done(null, user['@rid'])

	passport.deserializeUser (id, done) ->
		User.findById id, (err, user) ->
			delete user['password']
			done(err, user)


	localStrategyConfig = 
		usernameField: 'email'
		passwordField: 'password'
		passReqToCallback: true

	


	passport.use 'local-signup', new LocalStrategy localStrategyConfig, (req, email, password, done) ->
		console.log "In signup I have the following email: #{email}"
		process.nextTick () ->
			User.findByEmail email, (err, user) ->
				console.log "Lessee if theres a user: #{JSON.stringify(user)}"
				if user
					done null, false, req.flash('signupMessage', "That email is already taken")
				else
					newUser = new User(req.body)
					if (req.body.teamName)
						newUser.createUserAndTeam req.body.teamName, (err, u) ->
							# console.log "New User Callback with #{err}, #{JSON.stringify(u)}"
							if err
								throw err
							# # Lets figure out team business
							# else if u.teamCode
							# 	console.log "Will add you to the team : #{u.teamCode}"
							# else
							# 	console.log "Lets create a team for our dude"
							return done null, u
					else
						newUser.save (err, u) ->
							# console.log "New User Callback with #{err}, #{JSON.stringify(u)}"
							if err
								throw err
							# Lets figure out team business
							else if u.teamCode
								console.log "Will add you to the team : #{u.teamCode}"
							else
								console.log "Lets create a team for our dude"
							return done null, u


	passport.use 'local-login', new LocalStrategy localStrategyConfig, (req, email, password, done) ->
		User.findByEmail email, (err, user) ->
			console.log "Got a user in passport callback : #{JSON.stringify(user)}"
			if err
				done err
			if !user
				return done null, false, req.flash('loginMessage', 'No User Found')

			if !user.validPassword(password)
				return done null, false, req.flash('loginMessage', 'Wrong Password!!')

			return done null, user.data





