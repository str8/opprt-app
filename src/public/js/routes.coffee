'use strict'

routes = angular.module('opprtRoutes', ['ngRoute'])
  .config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
    
    $locationProvider.html5Mode true

    $routeProvider
      .when '/signup',
        templateUrl:'partials/signup.jade'
        controller:'UsersController'
      
      .when '/login',
        templateUrl: 'partials/login.jade'
        controller: 'UsersController'

      .when '/', 
      	templateUrl:'partials/index.jade'

      .when '/users/me',
        templateUrl: 'partials/profile.jade'
        controller: 'UsersController'

      .otherwise 
        redirectTo:'/'
    ]


