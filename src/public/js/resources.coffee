'use strict';

resources = angular.module 'opprtResources', []

resources.factory 'User', ($resource) ->
	$resource 'users/:id', {id: '@rid'}

